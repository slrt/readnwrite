% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/StringProvider.R
\name{FileStringProvider}
\alias{FileStringProvider}
\title{Each time it's interrogated, yields a new line of a file}
\usage{
FileStringProvider(filepath, encoding = "UTF-8", onEmpty = function()
  NULL)
}
\arguments{
\item{onEmpty}{when the line after the last is called upon, the string provider returns \code{onEmpty()} instead.}

\item{filePath}{the path to the file}
}
\description{
\code{FileStringProvider} yields a function without argument. When called, the function returns a new line of the file. When the end of file is reached, the function returns \code{onEmpty()}.
}
